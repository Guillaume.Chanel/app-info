from pylatex import Document, Section, Subsection, Package
from pylatex.utils import italic
from pylatex.base_classes import CommandBase, Arguments
import dominate as dom
import markdown
import pandas as pd
import sys
import argparse


class Converter:
    def convert(self, df):
        self.add_header()
        for proj in df.itertuples():
            self.append_project(proj)
        self.add_footer()
        return self.printable_document()

    def add_header(self):
        pass

    def add_footer(self):
        pass

    def append_project(self, tuple_proj):
        raise NotImplementedError('The converter must implement the append_project method')

    def printable_document(self):
        raise NotImplementedError('The converter must implement the printable_document method')


class TexConverter(Converter):
    def __init__(self):
        self.doc = Document(inputenc='utf8x')

    def append_project(self, proj):
        with self.doc.create(Section('Title {} - {}'.format(proj.id, proj.TITLE))):
            self.doc.append(italic('Personne responsable: {}'.format(proj.PI)))
            self.doc.append(italic('Contact: {} <{}>'.format(proj.NAME, proj.EMAIL)))
            self.doc.append(italic('Faculté: {}'.format(proj.FAC)))
            self.doc.append(italic('Département: {}'.format(proj.DEPT)))

            with self.doc.create(Subsection('Context')):
                self.doc.append(proj.CTX)

            with self.doc.create(Subsection('Description du projet')):
                self.doc.append(proj.PROJ)

    def printable_document(self):
        return self.doc.dumps()


class MDConverter(Converter):
    def __init__(self):
        self.doc = ''

    def append_project(self, proj):
        self.doc += '# Projet {} - {}\n'.format(proj.id, proj.TITLE)
        self.doc += '*Personne responsable: {}*\\\n'.format(proj.PI)
        self.doc += '*Contact: {} <<{}>>*\\\n'.format(proj.NAME, proj.EMAIL)
        self.doc += '*Faculté: {}*\\\n'.format(proj.FAC)
        self.doc += '*Département: {}*\n'.format(proj.DEPT)
        self.doc += '## Context\n{}\n'.format(proj.CTX)
        self.doc += '## Description du projet\n{}\n'.format(proj.PROJ)

    def printable_document(self):
        return self.doc


class HTMLConverter(Converter):
    def __init__(self):
        self.doc = dom.document(title='Projets pour le cours applications informatiques')
        self.md2html = markdown.Markdown(safe_mode=True, extensions= ['nl2br', 'sane_lists', 'smarty', 'mdx_urlize'])

    def add_header(self):
        self.doc.head.add(dom.tags.link(rel='stylesheet', href='style.css'))
        self.doc.head.add(dom.tags.meta(charset='UTF-8'))

    def append_project(self, proj):
        with self.doc:
            with dom.tags.div(id='project_{}'.format(proj.id), cls='project'):
                with dom.tags.div(cls='title'):
                    dom.tags.h1('Projet {} - {}'.format(proj.id, proj.TITLE))

                with dom.tags.div(cls='client'):
                    dom.tags.p('Personne responsable: {}'.format(proj.PI))
                    with dom.tags.p('Contact: {} '.format(proj.NAME), __pretty=False):
                        dom.util.text('<')
                        dom.tags.a(proj.EMAIL, href='mailto:// {}'.format(proj.EMAIL))
                        dom.util.text('>')
                    dom.tags.p('Faculté: {}'.format(proj.FAC))
                    dom.tags.p('Département: {}'.format(proj.DEPT))

                with dom.tags.div(cls='context'):
                    dom.tags.h2('Context')
                    dom.util.raw(self.md2html.convert(proj.CTX))

                with dom.tags.div(cls='description'):
                    dom.tags.h2('Description du projet')
                    dom.util.raw(self.md2html.convert(proj.PROJ))

    def add_footer(self):
        self.doc.add(dom.tags.script(src='script.js'))

    def printable_document(self):
        return self.doc


class MoodleConverter(Converter):
    def __init__(self):
        self.doc = pd.DataFrame(columns=['groupidnumber', 'groupname', 'description'])

    def append_project(self, proj):
        description = '*Personne responsable: {}*\n'.format(proj.PI)
        description += '*Contact: {} <<{}>>*\n'.format(proj.NAME, proj.EMAIL)
        description += '*Faculté: {}*\n'.format(proj.FAC)
        description += '*Département: {}*\n\n'.format(proj.DEPT)
        description += '## Context\n\n{}\n\n'.format(proj.CTX)
        description += '## Description du projet\n\n{}\n'.format(proj.PROJ)
        data = pd.DataFrame(
            {
                'groupidnumber': proj.id,
                'groupname': '{:02}'.format(proj.id) + ' - ' + proj.TITLE,
                'description': description
            },
            index=[0])
        self.doc = pd.concat((self.doc, data))

    def printable_document(self):
        return self.doc.to_csv(index=False)


if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description='Convert the csv version of the questionnaire to various formats. By default the result is displayed on stdout.')
    parser.add_argument('inputfile', type=argparse.FileType('r'), help='csv file in UTF-8 containing the questionnaire; - can be used for stdin')
    parser.add_argument('outputfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout, help='name of the output file, if not specified then outputs on stdout')
    parser.add_argument('-f', type=str, choices=['md', 'html', 'tex', 'moodle_csv'], default='md', help='format of the output file (default: %(default)s)')
    parser.add_argument('-r', help='reindex the projects from 1 to N according to their order in the csv', action='store_true')
    args = parser.parse_args()

    # Read file content
    df = pd.read_csv(args.inputfile)

    # Reindex projects if requested
    if args.r:
        df['id'] = df.index + 1

    # Apply convertion
    if args.f == 'md':
        converted_doc = MDConverter().convert(df)
    elif args.f == 'html':
        converted_doc = HTMLConverter().convert(df)
    elif args.f == 'tex':
        converted_doc = TexConverter().convert(df)
    elif args.f == 'moodle_csv':
        converted_doc = MoodleConverter().convert(df)

    # Write output (apprently FileTypes should not be closed)
    args.outputfile.write(converted_doc.__str__())

