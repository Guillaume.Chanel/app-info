/* ============================ */
/* Manage the accordion dynamic */
/* ============================ */

var acc = document.getElementsByClassName("title");
var i;
for (i = 0; i < acc.length; i++) {

  acc[i].addEventListener("click", function() {

    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the 3 following elements (client, context, description) */
    var panel = this.nextElementSibling;
    for (j = 0; j < 3; j++) {
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
      panel = panel.nextElementSibling;
    }
  });
}

/* ================================================================== */
/* Add a button in the top of the page to open / close all accordions */
/* ================================================================== */

var node = document.createElement("DIV");
node.classList.add("top");
open = document.createElement("DIV");
open.classList.toggle("toggle_all");
close = document.createElement("DIV");
close.classList.toggle("toggle_all");
node.appendChild(open);
node.appendChild(close);
var textOpen = document.createTextNode("open all projects");
var textClose = document.createTextNode("Close all projects");
open.appendChild(textOpen);
close.appendChild(textClose);


function toggle_all(type) {

  var acc = document.getElementsByClassName("title");
  if(type === "open") {
    for (i = 0; i < acc.length; i++) {
      acc[i].classList.add("active");
      var panel = acc[i].nextElementSibling;
      for (j = 0; j < 3; j++) {
        panel.style.display = "block";
        panel = panel.nextElementSibling;
      }
    }
  }
  else {
    for (i = 0; i < acc.length; i++) {
      acc[i].classList.remove("active");
      var panel = acc[i].nextElementSibling;
      for (j = 0; j < 3; j++) {
        panel.style.display = "none";
        panel = panel.nextElementSibling;
      }
    }
  }
}
open.addEventListener("click", function() {
  toggle_all("open")
});
close.addEventListener("click", function() {
  toggle_all("close")
});
document.body.prepend(node);
