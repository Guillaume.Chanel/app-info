import pyomo.environ as omo
import pandas as pd
import argparse
import numpy as np

def convert_limesurvey_to_df(csv_file):
    # Define student idenfying variables
    id_vars = ['NAME', 'MAIL', 'REM']

    # Read the csv file and place in panda dataframe
    in_df = pd.read_csv(csv_file)

    # Eliminate incomplete rows and reindex
    in_df.dropna(subset=['lastpage'], inplace=True)
    in_df.reset_index(inplace=True)

    # select columns containing the choices or the student identifying variables
    rank_cols = [c for c in in_df.columns if 'RANKPROJ[' in c]
    in_df = in_df[id_vars + rank_cols]

    # Find the number of projects
    max_proj_num = max([int(c.split('[')[1].split(']')[0]) for c in rank_cols])

    # Melt the df to obtain the rank of each project
    melted_df = in_df.melt(id_vars=id_vars, value_vars=rank_cols, var_name='rank', value_name='project')
    melted_df.dropna(subset=['project'], inplace=True)  # remove projects without choices

    # Convert the values in the table from string to integers (integer rank and integer number for project)
    melted_df['rank'] = melted_df['rank'].map(lambda x: int(x.split('[')[1].split(']')[0]))
    melted_df['project'] = melted_df['project'].map(lambda x: int(x[1:]))

    # Finally pivot the table to have projects as columns and rank as values (missing values are replaced by highest rank)
    df = melted_df.pivot_table(index=['NAME', 'MAIL'], columns='project', fill_value=max_proj_num)
    df.columns = df.columns.droplevel()
    df.reset_index(inplace=True)
    df.columns.name = None

    return df


# Parse arguments
parser = argparse.ArgumentParser(description='Use the student choices collected on limesurvey to perform project attribution')
parser.add_argument('inputfile', type=argparse.FileType('r'), help='csv file in UTF-8 containing the student choices')
parser.add_argument('outputfile', type=argparse.FileType('w'), help='csv file containing the results')
args = parser.parse_args()

minStudentPerProj = 1
maxStudentPerProj = 2

# Load the data in correct format
df = convert_limesurvey_to_df(args.inputfile)

model = omo.ConcreteModel()

# Define indexes of projects and students
model.STUDENTS = list(df.index)
nbProjects = int(df.columns[-1])
model.PROJECTS = list(range(1, nbProjects+1))  # WARN: assumes the last column contains the last project number and that projects are numbered starting by 1

def map_df(model, i, j):
    return df.loc[i,j]

model.preferences = omo.Param(model.STUDENTS, model.PROJECTS, initialize=map_df)  # WARN: also some strong assumptions on the df structure

# Define the potential number of students per project
model.COUNT_STUD = [0] + list(range(minStudentPerProj, maxStudentPerProj+1))

###
# Variable and domains

# Matrix on binary values determing if a student is associated with a project
model.student_proj = omo.Var(model.STUDENTS, model.PROJECTS, domain=omo.Binary)

# Vector indicating for each project how many students are associated to it
# This uses a reformulation to ensure that all variables are binary or integers (i.e. count_stud cannot be in the domain COUNT_STUD)
model.count_stud = omo.Var(model.PROJECTS, domain=omo.Integers)
model.select_count_stud = omo.Var(model.PROJECTS, model.COUNT_STUD, domain=omo.Binary)

###
# Constraints

# Ensure that count_stud variable is selected and instanciated
model.pick_count = omo.ConstraintList()
model.instanciate_count = omo.ConstraintList()
for p in model.PROJECTS:
    model.instanciate_count.add(
        model.count_stud[p] == sum(c*model.select_count_stud[p,c] for c in model.COUNT_STUD)
    )
    model.pick_count.add(
        sum(model.select_count_stud[p,c] for c in model.COUNT_STUD) == 1
    )

# There must be exactly one project per student
model.student_have_proj = omo.ConstraintList()
for s in model.STUDENTS:
    model.student_have_proj.add(sum(model.student_proj[s, p] for p in model.PROJECTS) == 1)

# The number of student per project must be the same as the count in the student_proj matrix
model.linkvars = omo.ConstraintList()
for p in model.PROJECTS:
    model.linkvars.add(sum(model.student_proj[s, p] for s in model.STUDENTS) == model.count_stud[p])


###
# Objective (minimize student disatisfaction and TODO minimize number of projects)
model.objective = omo.Objective(
    expr = sum(model.student_proj[s, p] * model.preferences[s, p] for s in model.STUDENTS for p in model.PROJECTS),
    sense = omo.minimize
)



# Solve the problem
solution = omo.SolverFactory('cbc').solve(model)
solution.write()

# Display and store solution + some stats
if solution['Solver'][0]['Status'].key == 'ok':
    for p in model.PROJECTS:
        for s in model.STUDENTS:
            if model.student_proj[s, p]():
                print('Student {}, {} is attributed to project {}'.format(s, df.loc[s,'MAIL'], p))
                df.loc[s, 'Attributed'] = p  # store result in dataframe

# define the rank of the attributed project
df['RankAttributed'] = np.diag(df[df['Attributed']].values)

print('objective = ', model.objective())

print('Number of students per project:')
print(df['Attributed'].value_counts())

print('Number of students by preference:')
print(df['RankAttributed'].value_counts())

# Save results
df.to_csv(args.outputfile)
args.outputfile.close()